import os
import awtk_locator as locator

def Helper(ARGUMENTS):
    locator.init(ARGUMENTS)

    from app_helper_base import AppHelperBase
    return AppHelperBase(ARGUMENTS)

def prepare_depends_libs(ARGUMENTS, helper, libs):
    if ARGUMENTS.get('PREPARE_DEPENDS', '').lower().startswith('f'):
        return

    args = ' AWTK_ROOT=' + helper.AWTK_ROOT

    if helper.MVVM_ROOT:
        args += ' MVVM_ROOT=' + helper.MVVM_ROOT

    if helper.BUILD_SHARED:
        args += ' SHARED=true'
    else:
        args += ' SHARED=false'

    if helper.LINUX_FB:
        args += ' LINUX_FB=true'
    else:
        args += ' LINUX_FB=false'

    for lib in libs:
        if os.path.exists(lib['root'] + '/SConstruct'):
            cmd = 'cd ' + lib['root'] + ' && scons' + args
            print(cmd)
            os.system(cmd)
